-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: freshme
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `catalogo`
--

DROP TABLE IF EXISTS `catalogo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `orden` smallint(6) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `padreId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E7AC6B453A5032F6` (`padreId`),
  CONSTRAINT `FK_E7AC6B453A5032F6` FOREIGN KEY (`padreId`) REFERENCES `catalogo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogo`
--

LOCK TABLES `catalogo` WRITE;
/*!40000 ALTER TABLE `catalogo` DISABLE KEYS */;
INSERT INTO `catalogo` VALUES (1,'C001','catalogo uno','asdasdas',1,1,NULL),(2,'C002','catalogo dos','dasdasdsa',1,1,NULL),(3,'C011','hijo uno','adasa',1,1,1);
/*!40000 ALTER TABLE `catalogo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `negocio_id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `orden` smallint(6) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4E10122D7D879E4F` (`negocio_id`),
  CONSTRAINT `FK_4E10122D7D879E4F` FOREIGN KEY (`negocio_id`) REFERENCES `negocio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,1,'Corte de pelo','dasdasd',1,1),(2,1,'Secar y peinar','dadasd',2,1),(3,1,'Secar y peinar','dadas',3,1),(5,2,'Cortecito','dedede',1,1),(6,3,'Cat1','Descripocip',1,1),(7,3,'Cat2','descripcion',2,1);
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cita`
--

DROP TABLE IF EXISTS `cita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) DEFAULT NULL,
  `servicio_id` int(11) DEFAULT NULL,
  `fecha` date NOT NULL,
  `horaInicio` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `horaFin` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `motivo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `calificacion` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3E379A62DB38439E` (`usuario_id`),
  KEY `IDX_3E379A6271CAA3E7` (`servicio_id`),
  CONSTRAINT `FK_3E379A6271CAA3E7` FOREIGN KEY (`servicio_id`) REFERENCES `servicio` (`id`),
  CONSTRAINT `FK_3E379A62DB38439E` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cita`
--

LOCK TABLES `cita` WRITE;
/*!40000 ALTER TABLE `cita` DISABLE KEYS */;
INSERT INTO `cita` VALUES (1,1,1,'2019-05-22','09:00','09:30',0,'fgdfdg',NULL),(2,1,1,'2019-05-31','09:00','09:30',0,'fgdfdg',NULL),(3,1,6,'2019-05-30','10:30','11:00',0,'Eliminar para prueba',NULL),(11,1,1,'2019-05-31','09:30','10:00',0,'fgdfdg',NULL),(12,1,2,'2019-05-31','09:30','10:00',0,'fgdfdg',NULL),(13,1,1,'2019-05-31','09:30','10:00',0,'fgdfdg',NULL),(14,1,2,'2019-05-31','09:30','10:00',0,'fgdfdg',NULL),(15,1,1,'2019-06-01','09:30','10:00',0,'fgdfdg',NULL),(16,1,2,'2019-06-01','09:30','10:00',0,'fgdfdg',NULL),(17,1,1,'2019-06-01','09:30','10:00',0,'fgdfdg',NULL),(18,1,2,'2019-06-01','09:30','10:00',0,'fgdfdg',NULL),(19,1,6,'2019-06-01','09:30','10:00',0,'dgdfgdfg',NULL),(20,1,2,'2019-06-06','10:30','',0,'fgdfdg',NULL),(21,1,3,'2019-06-06','10:30','',0,'',NULL),(22,16,2,'2019-06-12','12:00','',0,'cancelación de prueba',NULL),(23,16,3,'2019-06-12','12:00','',0,'cancelación de prueba',NULL),(24,16,4,'2019-06-12','12:00','',0,'',NULL),(25,1,2,'2019-07-02','9:00','',0,'fgdfdg',NULL),(26,1,5,'2019-07-27','11:00','',0,'fgdfdg',NULL),(27,1,2,'2019-07-27','11:00','',0,'',NULL),(28,1,5,'2019-07-27','11:00','',0,'',NULL),(29,1,4,'2019-07-27','11:00','',0,'',NULL),(30,1,3,'2019-07-27','11:00','',0,'',NULL),(31,1,2,'2019-07-27','11:00','',0,'',NULL),(32,1,4,'2019-07-27','10:00','',0,'fdfg',NULL),(33,1,3,'2019-07-27','10:00','',0,'',NULL),(34,1,2,'2019-07-27','10:00','',0,'fdfg',NULL),(35,1,4,'2019-07-27','1:00','',0,'',NULL),(36,1,5,'2019-07-27','1:00','',0,'',NULL),(37,1,3,'2019-07-27','1:00','',0,'',NULL),(38,1,2,'2019-07-27','10:00','',0,'',NULL),(39,1,3,'2019-07-27','10:00','',0,'',NULL),(40,1,4,'2019-07-27','10:00','',0,'',NULL),(41,1,5,'2019-07-27','10:00','',0,'',NULL),(42,1,3,'2019-07-27','10:00','',0,'',NULL),(43,1,2,'2019-07-27','10:00','',0,'',NULL),(44,1,4,'2019-07-27','10:00','',0,'',NULL),(45,1,6,'2019-07-27','12:00','',0,'',NULL),(46,1,2,'2019-07-28','11:00','',0,'retretre',NULL),(47,1,5,'2019-07-28','11:00','',0,'',NULL),(48,1,6,'2019-07-28','12:00','',0,'',NULL),(49,1,2,'2019-07-28','11:00','',0,'',NULL),(50,1,6,'2019-07-28','12:00','',0,'fguyr',NULL),(51,1,6,'2019-07-28','12:00','',0,'',NULL),(52,1,2,'2019-07-28','11:00','',0,'',NULL),(53,1,5,'2019-07-28','11:00','',0,'retretre',NULL),(54,1,4,'2019-07-28','11:00','',0,'retretre',NULL),(55,1,3,'2019-07-28','11:00','',0,'retretre',NULL),(56,1,6,'2019-07-28','12:00','',0,'fguyr',NULL),(57,1,6,'2019-07-28','2:00','',0,'fguyr',NULL),(58,1,6,'2019-07-28','12:00','',0,'fguyr',NULL),(59,1,2,'2019-07-28','11:00','',0,'retretre',NULL),(60,1,2,'2019-07-29','11:00','',0,'retretre',NULL),(61,1,5,'2019-07-29','11:00','',0,'retretre',NULL),(62,1,3,'2019-07-30','11:00','',0,'retretre',NULL),(63,1,4,'2019-07-30','11:00','',0,'retretre',NULL),(64,1,3,'2019-08-02','11:00','',1,'confirmado',NULL),(65,1,4,'2019-08-02','11:00','',1,'confirmado',NULL),(66,29,4,'2019-08-28','11:00','',0,'1234',NULL),(67,29,3,'2019-08-28','11:00','',0,'no puedo',NULL),(68,29,5,'2019-08-28','12:00','',0,'peinaod',NULL),(69,29,6,'2019-08-28','12:00','',0,'retertre',NULL),(70,29,6,'2019-08-28','11:00','',0,'',NULL),(71,30,2,'2019-08-28','10:00','',1,'confirmado',NULL),(72,30,3,'2019-08-28','10:00','',1,'confirmado',NULL),(74,29,5,'2019-08-29','10:00','',0,'123',NULL),(75,29,2,'2019-08-29','11:00','',0,'ped',NULL),(76,29,2,'2019-08-29','11:00','',0,'df',NULL),(77,29,2,'2019-08-29','10:00','',0,'1234',NULL),(78,29,2,'2019-08-29','10:00','',0,'',NULL),(81,29,5,'2019-09-07','10:00','',0,'',1),(82,29,2,'2019-09-07','11:00','',0,'1234',NULL),(83,29,2,'2019-09-11','11:00','',0,'',NULL),(84,29,2,'2019-09-11','10:00','',0,'Prueba',NULL),(85,33,2,'2019-09-15','10:00','',0,'Ccv',NULL),(86,33,3,'2019-09-15','10:00','',1,NULL,NULL),(87,33,5,'2019-09-15','10:00','',1,NULL,NULL),(88,33,2,'2019-09-15','10:00','',1,NULL,NULL),(89,33,3,'2019-09-15','10:00','',1,NULL,NULL),(90,29,2,'2019-09-19','11:00','',0,'',NULL),(91,29,6,'2019-09-21','11:00','',1,NULL,NULL),(92,29,2,'2019-09-22','2:00','',1,NULL,NULL),(93,29,5,'2019-09-22','2:00','',1,NULL,NULL),(94,16,2,'2019-09-23','12:00','',1,NULL,NULL),(95,36,2,'2019-09-23','10:00','',1,NULL,NULL),(96,36,3,'2019-09-23','10:00','',1,NULL,NULL);
/*!40000 ALTER TABLE `cita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `orden` smallint(6) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  `catalogoId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1F1B251E7DD3A7CE` (`catalogoId`),
  CONSTRAINT `FK_1F1B251E7DD3A7CE` FOREIGN KEY (`catalogoId`) REFERENCES `catalogo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `negocio`
--

DROP TABLE IF EXISTS `negocio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `negocio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_id` int(11) NOT NULL,
  `propietario_id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `calificacion` smallint(6) DEFAULT '0',
  `principal` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `secundaria` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `numeracion` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `telefonoMovil` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `telefonoFijo` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `latitud` decimal(10,8) NOT NULL,
  `longitud` decimal(10,8) NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `horarioInicio` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `horarioFin` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `activo` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `IDX_7528E379A9276E6C` (`tipo_id`),
  KEY `IDX_7528E37953C8D32C` (`propietario_id`),
  CONSTRAINT `FK_7528E37953C8D32C` FOREIGN KEY (`propietario_id`) REFERENCES `usuario` (`id`),
  CONSTRAINT `FK_7528E379A9276E6C` FOREIGN KEY (`tipo_id`) REFERENCES `tipo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `negocio`
--

LOCK TABLES `negocio` WRITE;
/*!40000 ALTER TABLE `negocio` DISABLE KEYS */;
INSERT INTO `negocio` VALUES (1,1,1,'Sofia Martin','33d4d5f8f6090fe1bd4d84b58c54212a.jpeg',0,'La coruña','6 de diciembre','E3-55','098949464','098949464',0.45400000,78.45500000,'asdasdas','09:00','15:00',1),(2,1,1,'Isidro','35b1ea473bb4aa83fca2332eb91a62e8.jpeg',0,'Iñaquito','10 de agosto','05-12','098954654','098954654',0.45640000,78.45645640,'asdasdas','10:00','17:00',1),(3,1,28,'Tijeras','27656ec6c31dcfa3adccb69266364e06.jpeg',0,'Jorge Drom','Gaspar','E3-99','0989498200','2250966',0.25600000,75.00000000,'Esta','12:00','16:00',1);
/*!40000 ALTER TABLE `negocio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil`
--

DROP TABLE IF EXISTS `perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permiso` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil`
--

LOCK TABLES `perfil` WRITE;
/*!40000 ALTER TABLE `perfil` DISABLE KEYS */;
INSERT INTO `perfil` VALUES (1,'propietario','Propietario de locales','admin'),(2,'usuario','Usuario normal','user'),(3,'administrador','Usuario administrador','super admin');
/*!40000 ALTER TABLE `perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicio`
--

DROP TABLE IF EXISTS `servicio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `costo` decimal(10,2) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CB86F22A3397707A` (`categoria_id`),
  CONSTRAINT `FK_CB86F22A3397707A` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicio`
--

LOCK TABLES `servicio` WRITE;
/*!40000 ALTER TABLE `servicio` DISABLE KEYS */;
INSERT INTO `servicio` VALUES (1,1,'afeitado a navaja','asdasdas',20.00,0),(2,1,'arreglo de barba','asdadas',25.00,1),(3,2,'peinado','adasd',23.00,1),(4,2,'recogido dia','aabgdfgdfg',26.00,1),(5,3,'peinado','asdabhdgdf',23.00,1),(6,5,'otro','adasd',20.00,1),(7,6,'servicio','se hace',12.00,1);
/*!40000 ALTER TABLE `servicio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo`
--

DROP TABLE IF EXISTS `tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci,
  `imagen` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `orden` smallint(6) NOT NULL,
  `activo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo`
--

LOCK TABLES `tipo` WRITE;
/*!40000 ALTER TABLE `tipo` DISABLE KEYS */;
INSERT INTO `tipo` VALUES (1,'Estilistas y Peluquerias',NULL,'estilistasPeluquerias.png',1,1),(2,'Manicure y Uñas',NULL,'manicurePintadoUnias.png',2,1),(3,'Barbería',NULL,'barberia.png',3,1),(4,'Spa',NULL,'spa.png',4,1),(5,'Centro estética',NULL,'centroEstetica.png',5,1),(6,'Cirugía Plástica',NULL,'cirugiaPlastica.png',6,1),(7,'Depilación',NULL,'depilacion.png',7,1),(8,'Maquillaje',NULL,'maquillaje.png',8,1),(9,'Micro Balding',NULL,'microblading_unias.png',9,1);
/*!40000 ALTER TABLE `tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perfil_id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `primerNombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `segundoNombre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primerApellido` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `segundoApellido` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nacimiento` date DEFAULT NULL,
  `telefonoMovil` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `telefonoFijo` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ciudad` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apiKey` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_2265B05D92FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_2265B05DA0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_2265B05DC05FB297` (`confirmation_token`),
  KEY `IDX_2265B05D57291544` (`perfil_id`),
  CONSTRAINT `FK_2265B05D57291544` FOREIGN KEY (`perfil_id`) REFERENCES `perfil` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,1,'prop1','prop1','kamusruben@gmail.com','kamusruben@gmail.com',1,'fVByVYSUcUryi2uJa3H0p59JfUMD1yPZwSctVef8WO8','DVixG/oZsEtNEXvhUu9nX+I/lw7Duk/SMetGG/ZcR0JGqja7RWcl2u2b1e+VOO4kc4ZPTfEQBdcfSk+Mbw6/xA==','2019-09-24 13:34:09','dada','2019-05-22 08:38:22','a:1:{i:0;s:10:\"ROLE_ADMIN\";}','Juanito','Primero','Nuevo','Modificado','1989-05-21','0894646545','0894646545','propietario.png','Tarija','La paz','K4furO7Gk0qFxH72MwgpmfSWHccZfYuVFs1T2WigHSY'),(2,2,'usuario','usuario','usuario@gmail.com','usuario@gmail.com',1,'BewknVZMBUBSnv1EjGFk/te3lmgQjhqoty5vG6L.Meg','xFzqPimvy0oU4Jw6HfQIjC3pusAFye/KdIJR2P+agtRfY4xltTtjf/Q2RavSwH4AYIO17KHdFYWPNSJhUe5cLQ==','2019-08-13 21:50:50','dasda','2019-05-22 09:00:24','a:1:{i:0;s:10:\"ROLE_SUPER\";}','Usuario','Primero','Usuario','Primero','1979-05-21','0945612311','0945612311','usuario.png','La Paz',NULL,'apikey2'),(6,1,'kamusruben@gmail.com','kamusruben@gmail.com','prop1@gmail.com','prop11@gmail.com',1,'MsGV5yG9Bh4384kT0pdjoynDITGCB36HWeJE5divUZ0','1Mip8nPs0E5JPxAD4p5DS8DbMNUH4GCg7Snmc/K2pLjR/55oWSTEibghTIZlVMKvqXRVVJORtxOwx7d2aKG1MQ==','2019-06-10 13:41:12','sdasdas','2019-08-21 12:01:35','a:1:{i:0;s:10:\"ROLE_USER\";}','Carlos','dasdsa','Herrera','dsasd','2014-01-01','0989498297','54564','usuario2.png','Tarija','asdas','aquiYRClp95M2gYM-smd9DPpM_B8HZAVCOxij67Lmnk'),(7,2,'usuario@freshme.app1','usuario@freshme.app1','usuario@freshme.app1','usuario@freshme.app1',1,'xUHI.CnYLTDK5JdsasnYh4e8GjSLZM81gy9Cg.tLkLI','q8M08cd7nv7j5AwzAlsOKFRyv1+TT5G7tkttTh5xWcwvtPuyRT/HoPs8wEKUO8TU9+SUkC8VY/zUvW5xVgMAMA==',NULL,NULL,NULL,'a:0:{}','Juan','','Benitez','','2019-06-12','','',NULL,NULL,NULL,NULL),(15,2,'rploaiza@gmail.com','rploaiza@gmail.com','rploaiza@gmail.com','rploaiza@gmail.com',1,'w.Ki2711jGvFjRGucMWNbllpfzMy0b.qJfoEaRXokpo','fXpbV0KLMySIXMA92RCCmv+3YQyO7EeGQUMGHJU0WHyi2kS/ipoZIcAZRbPYB7RRmtbtZvAceLx2KpcpguOMGg==',NULL,'rVQ0cpKn',NULL,'a:0:{}','Roberth','','Loaiza','',NULL,'','',NULL,NULL,NULL,'miUvYRClp95M2gYM-smd9DPpM_B8HZAVCOxij67Lmnk'),(16,2,'edyson65@gmail.com','edyson65@gmail.com','edyson65@gmail.com','edyson65@gmail.com',1,'N1YI.SGONLWa3gUXnxvAuyoe9nV0EPwJcEVmcqXlZy4','F/gdwRA3K287rURXHkCDYHlww7rqr9/+hskQBAbT22G/2efuYk/KZQ69SRXQbyuWE+Wb+RyAcFDH7UFWkXCv6g==',NULL,'recovered16',NULL,'a:0:{}','Edison','','De La Cruz','',NULL,'','',NULL,NULL,NULL,'5HlFDja1b8Vl2e8tqrGibOLavXUUE_XH3WVvyVIhpQE'),(25,1,'kamusruben1@gmail.com','kamusruben1@gmail.com','kamusruben1@gmail.com','kamusruben1@gmail.com',1,'YofH/wykg72JHf5ImlMsdDelxvvubR34ISs7MlSj7gY','nueva2','2019-08-27 22:23:52','user_registered',NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}','Carlos',NULL,'Herrera',NULL,'2014-01-01','0989498297',NULL,NULL,'Quito',NULL,NULL),(28,1,'kamus@gmail.com','kamus@gmail.com','kamus@gmail.com','kamus@gmail.com',1,'FrglhLemj1HmxrV2QAEDJHQBKMiknYDMRNDSs5t2mNo','97lAIxVHYu+kCNtloG5qnnyq3VE6HZ3sVMwUQ9Q1aFxDuMoNgfgVhNDHuv+Fh6xKklDLJtXRIbKhO4vJKhYQRw==','2019-08-28 05:32:46','kamus@gmail.com',NULL,'a:1:{i:0;s:10:\"ROLE_ADMIN\";}','Carlos',NULL,'Herrera',NULL,'2014-08-09','0989498297',NULL,NULL,'Quito',NULL,NULL),(29,2,'pauloaiza@hotmail.es','pauloaiza@hotmail.es','pauloaiza@hotmail.es','pauloaiza@hotmail.es',1,'NMe43uNFqSuo/Wa0BNBQ0WwHPK2Ot91otdqzpzb9KsA','InVbyGIGY381kGz92k4xM6xNyk8qK16tru2ErSO2WN6h7LGpbuIOB66hs7wzRw93oVcDw6tkR3CyJdooHg/1bw==',NULL,NULL,NULL,'a:0:{}','Roberth','','Loaiza','','2019-11-21','','',NULL,NULL,NULL,'xcl3dzLH1hJLiGE_QTXwD9UyM3V-WcM5owabUX9WnNE'),(30,2,'edison.delacruz@bayteq.com','edison.delacruz@bayteq.com','edison.delacruz@bayteq.com','edison.delacruz@bayteq.com',1,'JFme2sr8Q.bXw3/TWQg70F4eVgM6dp2.2C1KTOTSFUw','RRvBYOb/gjZ52VvtwPl4bL3BUzk5CUrx8xew5X2iVjt9DB2AFUhsChpUKJgpRgFphzuLl3/Oh8J6BCJNJ9k41w==',NULL,NULL,NULL,'a:0:{}','Tesy','','Juan','',NULL,'','',NULL,NULL,NULL,''),(33,2,'horajav.tabs@gmail.com','horajav.tabs@gmail.com','horajav.tabs@gmail.com','horajav.tabs@gmail.com',1,'InmTeQygAti4HblWJcEzhzJr4aRMe98ME9rBQACZ3k8','hKfXe+d83o1Y84FrUmj04Z8X+NI0MnSeD3dTmUJYvZ0+BS/HxvTpMNimF5L28Ow568S3+fjuCfD/yl/nC2KrOw==',NULL,NULL,NULL,'a:0:{}','Horacio','','ARDUZ','',NULL,'','',NULL,NULL,NULL,'wkoyS1W229exLvjwnm3xxfg80YVAiLeGPuX3iOvQvS4'),(34,2,'dfgd@dsf.com','dfgd@dsf.com','dfgd@dsf.com','dfgd@dsf.com',1,'/vU.IOWPMgVZZ2Mf2ywtMwavb5qxVcQVcR5D1dr0Bp8','UQ1cXXl+MzHYYrQgRobV1tO1AuJF7/emltufBgqKETTqYxWnlt4t1GHV89OVCNKduYiXlYO+BB+VJOl3pPR40g==',NULL,'recovered34',NULL,'a:0:{}','sdgfsdf','','fdgfdgfd','','2019-09-21','','',NULL,NULL,NULL,NULL),(35,2,'sdgfd@p.com','sdgfd@p.com','sdgfd@p.com','sdgfd@p.com',1,'4TzmzSG5ixFlAuqleXBameHqFukR6hbQbNY4LNw1l8s','PCvLj/fyp9G0BeoSHrf+QV3GEYmzEYlTeNtHR2t8tur0/yk39G3/YewKMfL26BHQNHSIIT4EEi/rlsmVxmtgIw==',NULL,NULL,NULL,'a:0:{}','dfgffdg','','fdgfdgf','','1998-09-22','','',NULL,NULL,NULL,NULL),(36,2,'hora.jav@gmail.com','hora.jav@gmail.com','hora.jav@gmail.com','hora.jav@gmail.com',1,'l0dv8qD4Q2Jbnm0gdcb6c4Jw0txtmQueYOSsNrvh.eI','92LxQzwLdKP+dpv4oD3w/04XlZhKXQfjlP6nOMhjmWvY6Zm0dktNhoCx0qf4GL8lV+PWiYR3iRqOWT2jFi+1mg==',NULL,NULL,NULL,'a:0:{}','Horacio ','','Arduz','','1989-03-02','','',NULL,NULL,NULL,'4UsbMmJRsB03erbGD0Bcj7zpIityUst46aIt-mJofA0');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-26 13:59:09
