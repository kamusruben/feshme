<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Negocio
 *
 * @ORM\Table(name="negocio")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NegocioRepository")
 */
class Negocio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Tipo")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $tipo;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $propietario;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255)
     */
    private $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="principal", type="string", length=50)
     */
    private $principal;
    /**
     * @var string
     *
     * @ORM\Column(name="secundaria", type="string", length=50)
     */
    private $secundaria;
    /**
     * @var string
     *
     * @ORM\Column(name="numeracion", type="string", length=10)
     */
    private $numeracion;

    /**
     * @var string
     *
     * @ORM\Column(name="telefonoMovil", type="string", length=10)
     */
    private $telefonoMovil;

    /**
     * @var string
     *
     * @ORM\Column(name="telefonoFijo", type="string", length=10)
     */
    private $telefonoFijo;

    /**
     * @var string
     *
     * @ORM\Column(name="latitud", type="decimal", precision=10, scale=8)
     */
    private $latitud;

    /**
     * @var string
     *
     * @ORM\Column(name="longitud", type="decimal", precision=10, scale=8)
     */
    private $longitud;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="horarioInicio", type="string", length=5)
     */
    private $horarioInicio;

    /**
     * @var string
     *
     * @ORM\Column(name="horarioFin", type="string", length=5)
     */
    private $horarioFin;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

    public function __toString() {
        return $this->getNombre();
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Negocio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Negocio
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set principal
     *
     * @param string $principal
     *
     * @return Negocio
     */
    public function setPrincipal($principal)
    {
        $this->principal = $principal;

        return $this;
    }

    /**
     * Get principal
     *
     * @return string
     */
    public function getPrincipal()
    {
        return $this->principal;
    }

    /**
     * Set secundaria
     *
     * @param string $secundaria
     *
     * @return Negocio
     */
    public function setSecundaria($secundaria)
    {
        $this->secundaria = $secundaria;

        return $this;
    }

    /**
     * Get secundaria
     *
     * @return string
     */
    public function getSecundaria()
    {
        return $this->secundaria;
    }

    /**
     * Set numeracion
     *
     * @param string $numeracion
     *
     * @return Negocio
     */
    public function setNumeracion($numeracion)
    {
        $this->numeracion = $numeracion;

        return $this;
    }

    /**
     * Get numeracion
     *
     * @return string
     */
    public function getNumeracion()
    {
        return $this->numeracion;
    }

    /**
     * Set telefonoMovil
     *
     * @param string $telefonoMovil
     *
     * @return Negocio
     */
    public function setTelefonoMovil($telefonoMovil)
    {
        $this->telefonoMovil = $telefonoMovil;

        return $this;
    }

    /**
     * Get telefonoMovil
     *
     * @return string
     */
    public function getTelefonoMovil()
    {
        return $this->telefonoMovil;
    }

    /**
     * Set telefonoFijo
     *
     * @param string $telefonoFijo
     *
     * @return Negocio
     */
    public function setTelefonoFijo($telefonoFijo)
    {
        $this->telefonoFijo = $telefonoFijo;

        return $this;
    }

    /**
     * Get telefonoFijo
     *
     * @return string
     */
    public function getTelefonoFijo()
    {
        return $this->telefonoFijo;
    }

    /**
     * Set latitud
     *
     * @param string $latitud
     *
     * @return Negocio
     */
    public function setLatitud($latitud)
    {
        $this->latitud = $latitud;

        return $this;
    }

    /**
     * Get latitud
     *
     * @return string
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * Set longitud
     *
     * @param string $longitud
     *
     * @return Negocio
     */
    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;

        return $this;
    }

    /**
     * Get longitud
     *
     * @return string
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Negocio
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set horarioInicio
     *
     * @param string $horarioInicio
     *
     * @return Negocio
     */
    public function setHorarioInicio($horarioInicio)
    {
        $this->horarioInicio = $horarioInicio;

        return $this;
    }

    /**
     * Get horarioInicio
     *
     * @return string
     */
    public function getHorarioInicio()
    {
        return $this->horarioInicio;
    }

    /**
     * Set horarioFin
     *
     * @param string $horarioFin
     *
     * @return Negocio
     */
    public function setHorarioFin($horarioFin)
    {
        $this->horarioFin = $horarioFin;

        return $this;
    }

    /**
     * Get horarioFin
     *
     * @return string
     */
    public function getHorarioFin()
    {
        return $this->horarioFin;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return Tipo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return bool
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Get tipo
     *
     * @return \AppBundle\Entity\Tipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set tipo
     *
     * @param \AppBundle\Entity\Tipo $tipo
     * @return Negocio
     */
    public function setTipo(\AppBundle\Entity\Tipo $tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }
    
    /**
     * Get Propietario
     *
     * @return \AppBundle\Entity\Usuario
     */
    public function getPropietario()
    {
        return $this->propietario;
    }

    /**
     * Set propietario
     *
     * @param \AppBundle\Entity\Usuario $propietario
     * @return Negocio
     */
    public function setPropietario(\AppBundle\Entity\Usuario $propietario)
    {
        $this->propietario = $propietario;

        return $this;
    }
}

