<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\File;

class NegocioType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('tipo')
        ->add('nombre')
        //->add('logo');
        ->add('logo', FileType::class, [
            'label' => 'Logotipo (JPG file)',
            'mapped' => false,
            'required' => false,
            'constraints' => [
                new File([
                    'maxSize' => '1024k',
                    'mimeTypes' => [
                        'image/jpeg',
                    ],
                    'mimeTypesMessage' => 'Ingrese una imagen JPG válida',
                ])
            ]
        ])
        //->add('calificacion')
        ->add('principal',TextType::class,['label'=>'Calle principal'])
        ->add('secundaria',TextType::class,['label'=>'Calle secundaria'])
        ->add('numeracion',TextType::class,['label'=>'Numeración'])
        ->add('telefonoMovil')
        ->add('telefonoFijo')
        ->add('latitud',TextType::class, array('attr'=>array('readonly' => true)))
        ->add('longitud',TextType::class, array('attr'=>array('readonly' => true)))
        ->add('descripcion')
        ->add('horarioInicio')
        ->add('horarioFin')
        ;
        //->add('propietario');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Negocio'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_negocio';
    }


}
