<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;

class NegocioEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('tipo',EntityType::class, [ 'class' => 'AppBundle:Tipo','attr'=>['readonly'=>true]])
        ->add('nombre',TextType::class, array('attr'=>array('readonly' => true)))
            ->add('logo', FileType::class, [
                'label' => 'Logotipo (JPG file)',
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Ingrese una imagen JPG válida',
                    ])
                ]
            ])
        ->add('principal')
        ->add('secundaria')
        ->add('numeracion')
        ->add('telefonoMovil')
        ->add('telefonoFijo')
        ->add('latitud',TextType::class, array('attr'=>array('readonly' => true)))
        ->add('longitud',TextType::class, array('attr'=>array('readonly' => true)))
        ->add('descripcion')
        ->add('horarioInicio')
        ->add('horarioFin')
        ;
        //->add('propietario');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Negocio'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_negocio';
    }


}
