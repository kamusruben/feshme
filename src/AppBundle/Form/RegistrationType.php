<?php
// src/AppBundle/Form/RegistrationType.php
namespace AppBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        //$resolver->setRequired('entity_manager');
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
         
        $builder->add('primerNombre')->add('segundoNombre')->add('primerApellido')->add('segundoApellido')
        ->add('nacimiento')->add('telefonoMovil')->add('telefonoFijo')->add('direccion')->add('ciudad')
        ->add('perfil');
    }
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }
    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
    public function getName()
    {
        return $this->getBlockPrefix();
    }
}