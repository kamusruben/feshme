<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class UsuarioEditType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        //->add('email')
        ->add('primerNombre')
        ->add('segundoNombre')
        ->add('primerApellido')
        ->add('segundoApellido')
        ->add('nacimiento',DateType::Class, array(
                'widget' => 'choice',
                'years' => range(date('Y'), date('Y')-60),
                'months' => range(date('m'), 12),
                'days' => range(date('d'), 31),
            ))
        ->add('telefonoMovil')
        ->add('ciudad')
        ->add('direccion')
        ->add('telefonoFijo');
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Usuario'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_usuario';
    }


}
