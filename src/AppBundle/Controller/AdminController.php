<?php

namespace AppBundle\Controller;

use AppBundle\Security\ApiKeyAuthenticator;
use AppBundle\Security\ApiKeyUserProvider;
use AppBundle\Security\RestResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdminController extends Controller
{
    private $rest;

    public function __construct(RestResponse $rest) {
        $this->rest = $rest;
    }
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $tipos= $em->getRepository('AppBundle:Tipo')->findAll();
        $negocios= $em->getRepository('AppBundle:Negocio')->todosAdmin();
        // replace this example code with whatever you need
        return $this->render('admin/index.html.twig', [
            'nombre' => 'Administrador',
            'tipos' => $tipos,
            'negocios' => $negocios,
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
    public function activarTipoAction(Request $request){
        $status = 200;
        $data = json_decode(
            $request->getContent(),
            true
        );
        if(array_key_exists ('id',$data) && array_key_exists ('value',$data)){
            $em = $this->getDoctrine()->getManager();
            $tipo = $em->getRepository('AppBundle:Tipo')->find($data['id']);
            $tipo->setActivo($data['value']);
            $em->flush();
            $respuesta = $this->rest->response(true,$status,'','');
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Cita');
        }
        return new JsonResponse($respuesta,$status);
    }
    public function activarNegocioAction(Request $request){
        $status = 200;
        $data = json_decode(
            $request->getContent(),
            true
        );
        if(array_key_exists ('id',$data) && array_key_exists ('value',$data)){
            $em = $this->getDoctrine()->getManager();
            $tipo = $em->getRepository('AppBundle:Negocio')->find($data['id']);
            $tipo->setActivo($data['value']);
            $em->flush();
            $respuesta = $this->rest->response(true,$status,'','');
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Cita');
        }
        return new JsonResponse($respuesta,$status);
    }
}