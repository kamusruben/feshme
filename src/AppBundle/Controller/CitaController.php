<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Usuario;
use AppBundle\Entity\Cita;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Security\ApiKeyAuthenticator;
use AppBundle\Security\ApiKeyUserProvider;
use AppBundle\Security\RestResponse;
/**
 * Cita controller.
 *
 */
class CitaController extends Controller
{
    private $apiAuth;
    private $apiUser;
    private $rest;

    public function __construct(ApiKeyAuthenticator $apiAuth, ApiKeyUserProvider $apiUser, RestResponse $rest) {
        $this->apiAuth = $apiAuth;
        $this->apiUser = $apiUser;
        $this->rest = $rest;
    }
    /**
     * Información de la cita
     *
     */
    public function listadoPorUsuarioAction(Request $request)
    {
        $status = 200;
        $apiKey = $request->headers->get('x-apiKey');
        $em = $this->getDoctrine()->getManager();
        $usuario = $this->apiUser->getUsernameForApiKey($apiKey,$em,true);
        if(count($usuario)>0){
            $citas = $em->getRepository('AppBundle:Cita')->porUsuarioSoloCitas($usuario[0]->getId());
            $salida = array();
            if(count($citas)>0){
//                foreach($citas as $cita){
//                    $existe = false;
//                    $index = count($salida);
//                    if(count($salida)>0){
//                        foreach($salida as $key => $sal){
//                            if($sal['negocio']['negocioId']==$cita['negocioId']){
//                                $existe = true;
//                                $index = $key;
//                            }
//                        }
//                    }
//                    if(!$existe){
//                        $salida[$index]['id'] = $cita['id'];
//                        $salida[$index]['negocio']=array(
//                            'negocioId' => $cita['negocioId'],
//                            'nombre' => $cita['negocioNombre'],
//                            'imagen' => $cita['negocioImagen'],
//                            'principal' => $cita['negocioPrincipal'],
//                            'secundaria' => $cita['negocioSecundaria'],
//                            'numeracion' => $cita['negocioNumeracion'],
//                            'citas' => array(0=>array('id' => $cita['id'],
//                                'fecha' => $cita['fecha'],
//                                'horaInicio' => $cita['horaInicio'],
//                                'horaFin' => $cita['horaFin'],
//                                'costo' => $cita['costo'],
//                                'nombre' => $cita['nombre']))
//                        );
//                    }else{
//                        $salida[$index]['negocio']['citas'][] = array('id' => $cita['id'],
//                                'fecha' => $cita['fecha'],
//                                'horaInicio' => $cita['horaInicio'],
//                                'horaFin' => $cita['horaFin'],
//                                'costo' => $cita['costo'],
//                                'nombre' => $cita['nombre']);
//                    }
//                }
                $respuesta = $this->rest->response($citas,200,'001','');
            }else{
                $respuesta = $this->rest->response([],400,'001','Cita');
            }
        }else{
            $respuesta = $this->rest->response([],400,'001','Usuario');
        }
        return new JsonResponse($respuesta);
    }
    public function agregarAction(Request $request){
        $status = 200;
        $data = json_decode(
            $request->getContent(),
            true
        );
        $apiKey = $request->headers->get('x-apiKey');
        $em = $this->getDoctrine()->getManager();
        $usuario = $this->apiUser->getUsernameForApiKey($apiKey,$em,true);
        $completo = true;
        $citasCreadas = array();
        if(count($usuario)>0){
            foreach($data['servicioId'] as $id){
                $servicio = $em->getRepository('AppBundle:Servicio')->find($id);
                if(!is_null($servicio)){
                    $cita = new Cita();
                    $cita->setFecha(new \DateTime($data['fecha']));
                    $cita->setHoraInicio($data['horaInicio']);
                    $cita->setHoraFin($data['horaFin']);
                    $cita->setUsuario($usuario[0]);
                    $cita->setServicio($servicio);
                    $cita->setEstado(true);
                    $em->persist($cita);
                    $citasCreadas[] = $cita;
                    /*
                    //if ($form->isSubmitted() && $form->isValid()) {
                    if ($form->isSubmitted()) {
                        $em->persist($tipo);
                        $em->flush();
                        $respuesta = array(
                            'data' => 'ok',
                            'error' => []
                        );
                    }else{
                        $status = 400;
                        $respuesta = array(
                            'error' => array('codigo'=>'001','descripcion'=>'Información no encontrada')
                        );   
                    }*/
                }else{
                    $status = 400;
                    $respuesta = $this->rest->response([],$status,'002','Cita');
                    /*$respuesta = array(
                        'error' => array('codigo'=>'002','descripcion'=>'Servicio no existe: '.$id)
                    );*/   
                    $completo = false;
                    break;
                }
            }
            if($completo){
                $em->flush();
                $ids = array();
                foreach($citasCreadas as $cc){
                    $ids[] = $cc->getId();
                }
                /*$respuesta = array(
                    'data' => $ids,
                    'error' => []
                );*/
                $respuesta = $this->rest->response($ids,$status,'','');
            }
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Usuario');
            /*$respuesta = array(
                'error' => array('codigo'=>'001','descripcion'=>'Usuario no existe')
            );*/ 
        }
        /*$csrfProvider = $this->container->get('form.csrf_provider');
        $csrfToken = $csrfProvider->generateCsrfToken('unknown');
        var_dump($csrfToken);*/

        
        return new JsonResponse($respuesta,$status);
    }
    public function eliminarAction(Request $request){
        $status = 200;
        $data = json_decode(
            $request->getContent(),
            true
        );        
        $em = $this->getDoctrine()->getManager();
        $apiKey = $request->headers->get('x-apiKey');
        $usuario = $this->apiUser->getUsernameForApiKey($apiKey,$em,false);
        if(count($usuario)>0){
            if(is_array($data['id'])){
                $corresponden = true;
                foreach($data['id'] as $citaId){
                    $cita = $em->getRepository('AppBundle:Cita')->find($citaId);
                    if(!is_null($cita) && $cita->getUsuario()->getId() == $usuario[0]['id']){
                        $cita->setEstado(false);
                        $cita->setMotivo($data['motivo']);
                    }else{
                        $corresponden = false;
                        break;       
                    }
                }
                if($corresponden){
                    $em->flush();
                    $respuesta = $this->rest->response(true,$status,'','');
                }else{
                    $status = 400;
                    $respuesta = $this->rest->response([],$status,'001','Cita');
                }
            }else{
                $status = 400;
                $respuesta = $this->rest->response([],$status,'001','Cita');    
            }
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Usuario');
        }
        return new JsonResponse($respuesta,$status);
    }
    public function negocioAction(){
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $negocio = $em->getRepository('AppBundle:Negocio')->findOneBy(array('propietario'=>$user->getId()));
        $citas = $em->getRepository('AppBundle:Cita')->porNegocio($negocio->getId(),true);
        return $this->render('cita/list.html.twig', array(
            'nombre'      => $negocio->getNombre(),
            'negocio'     => $negocio,
            'citas'       => $citas,
        ));
    }
    public function pendientesCalificarAction(Request $request){
        $status = 200;
        $data = json_decode(
            $request->getContent(),
            true
        );        
        $em = $this->getDoctrine()->getManager();
        $apiKey = $request->headers->get('x-apiKey');
        $usuario = $this->apiUser->getUsernameForApiKey($apiKey,$em,false);
        if(count($usuario)>0){
            $respuesta = $em->getRepository('AppBundle:Cita')->citasPendientes($usuario[0]['id']);
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Usuario');
        }
        return new JsonResponse($respuesta,$status);
    }
    public function CalificarAction(Request $request){
        $status = 200;
        $data = json_decode(
            $request->getContent(),
            true
        );        
        $em = $this->getDoctrine()->getManager();
        $apiKey = $request->headers->get('x-apiKey');
        $usuario = $this->apiUser->getUsernameForApiKey($apiKey,$em,false);
        if(count($usuario)>0){
            if(is_array($data['id'])){
                $corresponden = true;
                $bad = false;
                foreach($data['id'] as $citaId){
                    $cita = $em->getRepository('AppBundle:Cita')->find($citaId);
                    if(!is_null($cita) && $cita->getUsuario()->getId() == $usuario[0]['id']){
                        //$cita->setEstado(false);
                        if(!is_int($data['calificacion']) || $data['calificacion'] > 5){
                            $bad = true;
                        }else{
                            $cita->setCalificacion($data['calificacion']);
                        }
                    }else{
                        $corresponden = false;
                        break;       
                    }
                }
                if($corresponden && !$bad){
                    $em->flush();
                    $respuesta = $this->rest->response(true,$status,'','');
                }else if($bad){
                    $status = 400;
                    $respuesta = $this->rest->response([],$status,'009','');
                }else{
                    $status = 400;
                    $respuesta = $this->rest->response([],$status,'001','Cita');
                }
            }else{
                $status = 400;
                $respuesta = $this->rest->response([],$status,'001','Cita');    
            }
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Usuario');
        }
        return new JsonResponse($respuesta,$status);
    }
    public function confirmarAction(Request $request){
        $status = 200;
        $data = json_decode(
            $request->getContent(),
            true
        );
        if(array_key_exists ('id',$data) && array_key_exists ('value',$data)){
            $em = $this->getDoctrine()->getManager();
            $cita = $em->getRepository('AppBundle:Cita')->find($data['id']);
            $cita->setMotivo($data['value']);
            $cita->setEstado(true);
            $em->flush();
            $respuesta = $this->rest->response(true,$status,'','');
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Cita');
        }
        return new JsonResponse($respuesta,$status);
    }
}
