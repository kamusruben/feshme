<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Security\ApiKeyAuthenticator;
use AppBundle\Security\ApiKeyUserProvider;
use AppBundle\Security\RestResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Usuario controller.
 *
 */
class UsuarioController extends Controller
{
    private $apiAuth;
    private $apiUser;
    private $rest;

    public function __construct(ApiKeyAuthenticator $apiAuth, ApiKeyUserProvider $apiUser, 
        RestResponse $rest) {
        $this->apiAuth = $apiAuth;
        $this->apiUser = $apiUser;
        $this->rest = $rest;
    }
    /**
     * Lists all usuario entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $usuarios = $em->getRepository('AppBundle:Usuario')->findAll();

        return $this->render('usuario/index.html.twig', array(
            'usuarios' => $usuarios,
        ));
    }

    /**
     * Creates a new usuario entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $perfil = $em->getRepository('AppBundle:Perfil')->find(1);
        $usuario = new Usuario();
        $usuario->setPerfil($perfil);
        $usuario->setUsername('xxx');
        $usuario->setTelefonoFijo('');
        $usuario->setSegundoApellido('');
        $usuario->setSegundoNombre('');
        $form = $this->createForm('AppBundle\Form\UsuarioType', $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usuario->setUsername($usuario->getEmail());
            $usuario->setPlainPassword($usuario->getPassword());
            $usuario->setRoles(['ROLE_ADMIN']);
            $usuario->setEnabled(true);
            $em->persist($usuario);
            $em->flush();

            //return $this->redirectToRoute('negocio_new');
            return $this->redirectToRoute('usuario_show', array('id' => $usuario->getId()));
        }

        return $this->render('usuario/new.html.twig', array(
            'usuario' => $usuario,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a usuario entity.
     *
     */
    public function showAction(Usuario $usuario)
    {
        $deleteForm = $this->createDeleteForm($usuario);

        return $this->render('usuario/show.html.twig', array(
            'usuario' => $usuario,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing usuario entity.
     *
     */
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $usuario = $this->get('security.token_storage')->getToken()->getUser();
        $pass = $usuario->getPassword();
        $nego= $em->getRepository('AppBundle:Negocio')->findOneBy(array('propietario'=>$usuario->getId()));
        $nombre = is_null($nego) ? $usuario->getNombre(): $nego->getNombre();
        $deleteForm = $this->createDeleteForm($usuario);
        $editForm = $this->createForm('AppBundle\Form\UsuarioEditType', $usuario);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
//                $usuario->setPassword($pass);
//            die('pasa'.$usuario->getPassword());
//            echo '<pre>';
//                dump($usuario);
//            echo '</pre>';
//            die();
            $usuario->setConfirmationToken(null);
            $em->persist($usuario);
            $em->flush();
//            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home');
        }

        return $this->render('usuario/edit.html.twig', array(
            'nombre' => $nombre,
            'usuario' => $usuario,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a usuario entity.
     *
     */
    public function deleteAction(Request $request, Usuario $usuario)
    {
        $form = $this->createDeleteForm($usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($usuario);
            $em->flush();
        }

        return $this->redirectToRoute('usuario_index');
    }

    /**
     * Creates a form to delete a usuario entity.
     *
     * @param Usuario $usuario The usuario entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Usuario $usuario)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuario_delete', array('id' => $usuario->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    /**
     * Información del usuario
     *
     */
    public function infoAction(Request $request)
    {
        $apiKey = $request->headers->get('x-apiKey');
        $em = $this->getDoctrine()->getManager();
        $usuario = $this->apiUser->getUsernameForApiKey($apiKey,$em,false);
        $status = 200;
        if(count($usuario)>0){
            $entity = $em->getRepository('AppBundle:Usuario')->infoUsuario($usuario[0]['id']);
            if(count($entity)>0){
                $respuesta = $this->rest->response($entity,$status,'','');
            }else{
                $status = 400;
                $respuesta = $this->rest->response([],$status,'001','Usuario');
            }
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Usuario');
        }
        return new JsonResponse($respuesta,$status);
    }
    public function loginAction(Request $request){
        $data = json_decode(
            $request->getContent(),
            true
        );
        $usuario = $data['username'];
        $pass = $data['password'];
        $status = 200;
        $user_manager = $this->get('fos_user.user_manager');
        $user = $user_manager->findUserByUsername($usuario);
        if(!is_null($user)){
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $salt = $user->getSalt();
            $ok = $encoder->isPasswordValid($user->getPassword(), $pass, $salt);
            if($ok == false) {
                $status = 403;
                $respuesta = $this->rest->response([],$status,'005','');
            }else{
                if($user->getApiKey() != ''){
                    $token = $user->getApiKey();
                }else{
                    $token = $this->apiAuth->generateToken();
                    $user->setApiKey($token);
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();   
                }
                $respuesta = $this->rest->response($token,$status,'','');
            }
        }else{
            $status = 403;
            $respuesta = $this->rest->response([],$status,'005','');
        }
        return new JsonResponse($respuesta,$status);
    }
    public function logoutAction(Request $request){
        $status = 200;
        $data = json_decode(
            $request->getContent(),
            true
        );
        $em = $this->getDoctrine()->getManager();
        $apiKey = $request->headers->get('x-apiKey');
        $usuario = $this->apiUser->getUsernameForApiKey($apiKey,$em,true);
        if(count($usuario)>0){
            $usuario[0]->setApiKey('');
            $em->flush();
            $respuesta = $this->rest->response(true,$status,'','');
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Usuario');
        }
        return new JsonResponse($respuesta,$status);
    }
    public function registerAction(Request $request){
        $data = json_decode(
            $request->getContent(),
            true
        );
        //$usuario = $data['email'];
        //$pass = $data['password'];
        //$this->userManipulator->create('$username', '$password', '$email', true, true);
        $status = 200;
        $em = $this->getDoctrine()->getManager();
        $usuario = new Usuario();
        
        try{
            $existe = $em->getRepository('AppBundle:Usuario')->existeEmail($data['email']);
            $passOK = $data['password'] === $data['passwordConfirm'];
            if(!$existe && $passOK){
                $perfil = $em->getRepository('AppBundle:Perfil')->find(2);
                $usuario->setPerfil($perfil);
                $usuario->setUsername($data['email']);
                $usuario->setUsernameCanonical($data['email']);
                $usuario->setEmail($data['email']);
                $usuario->setEmailCanonical($data['email']);
                $usuario->setEnabled(true);
                $usuario->setPlainPassword($data['password']);
                //$usuario->setRoles(['ROLE_USER']);
                $usuario->setRoles(['ROLE_USER']);
                $usuario->setPrimerNombre($data['primerNombre']);
                $usuario->setSegundoNombre('');
                $usuario->setPrimerApellido($data['primerApellido']);
                $usuario->setSegundoApellido('');
                $usuario->setNacimiento(new \DateTime($data['nacimiento']));
                $usuario->setTelefonoMovil($data['telefonoMovil']);
                $usuario->setTelefonoFijo('');
                $usuario->setCiudad($data['ciudad']);
                $em->persist($usuario);
                $em->flush();

                //$user->addRole("ROLE_ADMIN");
                /*$user = $em->getRepository('AppBundle:Usuario')->find($usuario->getId());
                $user->addRole('ROLE_USER');
                $em->persist($user);
                $em->flush();*/


                $respuesta = $this->rest->response(['ok'=>true,'id'=>$usuario->getId()],$status,'','');
            }else{
                if($existe){   
                    $status = 403; 
                    $respuesta = $this->rest->response([],$status,'006','');
                }
                if(!$passOK){
                    $status = 403; 
                    $respuesta = $this->rest->response([],$status,'007','');
                }
            }
        }catch(\Exception $e){
            $msg = 'Existe un error interno '.$e->getMessage();
            $status = 500;
            $respuesta = $this->rest->response([],$status,'005',$msg);
        }
        return new JsonResponse($respuesta,$status);
    }
    public function actualizarAction(Request $request){
        $status = 200;
        $data = json_decode(
            $request->getContent(),
            true
        ); 
        $em = $this->getDoctrine()->getManager();
        $apiKey = $request->headers->get('x-apiKey');
        $usuario = $this->apiUser->getUsernameForApiKey($apiKey,$em,true);
        if(count($usuario)>0){
            $user = $usuario[0];

            /**
             * Campos obligatorios
             */
            $obligatosios = array('primerNombre','primerApellido','telefonoMovil','direccion','ciudad','nacimiento');
            $campos = $this->verificarObjeto($obligatosios,$data);
            if($campos===true){
                if($data['primerNombre'] != '' && array_key_exists('primerNombre',$data))
                    $user->setPrimerNombre($data['primerNombre']);
                if($data['segundoNombre'] != '')
                    $user->setSegundoNombre($data['segundoNombre']);
                if($data['primerApellido'] != '')
                    $user->setPrimerApellido($data['primerApellido']);
                if($data['segundoApellido'] != '')
                    $user->setSegundoApellido($data['segundoApellido']);
                if($data['telefonoMovil'] != '')
                    $user->setTelefonoMovil($data['telefonoMovil']);
                if($data['telefonoFijo'] != '')
                    $user->setTelefonoFijo($data['telefonoFijo']);
                if($data['direccion'] != '')
                    $user->setDireccion($data['direccion']);
                if($data['ciudad'] != '')
                    $user->setCiudad($data['ciudad']);
                if($data['nacimiento'] != '')
                    $user->setNacimiento( new \DateTime($data['nacimiento']));
                if($data['imagen'] != ''){
                    $nombreImagen = strlen($user->getImagen())==0 || is_null($user->getImagen())
                        ? 'usuario'.str_pad($user->getId(), 6, '0', STR_PAD_LEFT).'.jpg'
                        : $user->getImagen();
                    $img64 = base64_decode($data['imagen']);
                    $im = imagecreatefromstring($img64);
                    if ($im !== false) {
                        header('Content-Type: image/jpeg');
                        imagejpeg($im, $this->getParameter('avatar_directory')."/".$nombreImagen);
                        imagedestroy($im);
                    }
                    $user->setImagen($nombreImagen);
                }
                $em->persist($user);
                $em->flush();
                $respuesta = $this->rest->response(true,$status,'','');
            }else{
                $status = 400;
                $respuesta = $this->rest->response([],$status,'008',$campos);
            }
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Usuario');
        }
        return new JsonResponse($respuesta,$status);
    }
    public function cambiarClaveAction(Request $request){
        $status = 200;
        $data = json_decode(
            $request->getContent(),
            true
        );
        $em = $this->getDoctrine()->getManager();
        $apiKey = $request->headers->get('x-apiKey');
        $usuario = $this->apiUser->getUsernameForApiKey($apiKey,$em,true);
        if(count($usuario)>0){
            if($data['password']==$data['passwordConfirm']){
                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder($usuario[0]);
                $salt = $usuario[0]->getSalt();
                $ok = $encoder->isPasswordValid($usuario[0]->getPassword(), $data['actual'], $salt);
                if($ok === true) {
                    $user = $em->getRepository('AppBundle:Usuario')->find($usuario[0]->getId());
                    $password = $encoder->encodePassword($data['password'], $salt);
                    $user->setPassword($password);
                    $em->persist($user);
                    $em->flush();
                    $respuesta = $this->rest->response(true,$status,'','');
                }else{
                    $status = 403;
                    $respuesta = $this->rest->response([],$status,'005','');
                }
            }else{
                $status = 400;
                $respuesta = $this->rest->response([],$status,'007','');    
            }
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Usuario');
        }
        return new JsonResponse($respuesta,$status);
    } 
    public function recuperarAction(Request $request, $token){

        $em = $this->getDoctrine()->getManager();
        $usuarios = $em->getRepository('AppBundle:Usuario')->porToken($token);
        if(count($usuarios)>0){
            $userId = $usuarios[0]['id'];
            $usuario = $em->getRepository('AppBundle:Usuario')->find($userId);
            $form = $this->createForm('AppBundle\Form\PassType', $usuario);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $usuario->setPlainPassword($usuario->getPassword());
                $usuario->setConfirmationToken('recovered'.$usuario->getId());
                $em->persist($usuario);
                $em->flush();
                //return $this->redirectToRoute('negocio_new');
                return $this->redirectToRoute('usuario_actualizado_pass',array('userId' => $userId));
            }

            return $this->render('usuario/actualizar.html.twig', array(
                'usuario' => $usuario,
                'token' => $token,
                'form' => $form->createView(),
            ));

        }else{
            return $this->redirectToRoute('fos_user_security_login');
        }
    }
    public function actualizadoPassAction(int $userId){
        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository('AppBundle:Usuario')->find($userId);
        if($usuario->getConfirmationToken() == 'recovered'.$usuario->getId()){
            return $this->render('usuario/recuperado.html.twig', array(
                'usuario' => $usuario
            ));
        }else{
            return $this->redirectToRoute('error_404');
        }
    }
    public function recuperarClaveAction(Request $request){
        $status = 200;
        $data = json_decode(
            $request->getContent(),
            true
        );
        $em = $this->getDoctrine()->getManager();
        $usuarios = $em->getRepository('AppBundle:Usuario')->porEmail($data['email']);
        $status = 200;
        if(count($usuarios)>0){
            $user = $em->getRepository('AppBundle:Usuario')->find($usuarios[0]['id']);
            $password = $this->apiAuth->generatePassword();

            $user->setConfirmationToken($password);
            $em->persist($user);
            $em->flush();

            /*$factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $salt = $user->getSalt();
            $pass = $encoder->encodePassword($password, $salt);
            $user->setPassword($pass);
            $user->setPasswordRequestedAt(new \DateTime('now'));
            $user->setConfirmationToken($password);
            $em->persist($user);
            $em->flush();*/
            
            /*$dataEmail = array('token'=>$password);
            $this->sendEmail('recuperarPass',$usuarios[0]['email'],$dataEmail);
            $respuesta = $this->rest->response(true,$status,'','');*/
            $url = $this->container->get('router')->generate(
                'usuario_recuperar',
                ['token' => $password]
            );
            $respuesta = $this->rest->response($url,$status,'','');
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Usuario');
        }
        return new JsonResponse($respuesta,$status);
    }  
    private function verificarObjeto($campos,$objeto){
        $completo = true;
        foreach($campos as $campo){
            if(!array_key_exists($campo,$objeto)){
                $completo = $campo;
            }
        }
        return $completo;
    }
    private function sendEmail(string $template, string $email, array $data){
        $message = (new \Swift_Message('Hello Email'))
        ->setSubject('Recuperación de clave de acceso')
        ->setFrom('freshmeappdeveloper@gmal.com')
        ->setTo($email)
        ->setCharset('UTF-8')
        ->setBody(
            $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                'Email/'.$template.'.html.twig',
                $data
            ),
            'text/html'
        );  
        $this->get('mailer')->send($message);
    }
    public function olvideAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $usuario = new Usuario();
        $form = $this->createForm('AppBundle\Form\OlvideType', $usuario);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = $usuario->getEmail();

            $usuario = $em->getRepository('AppBundle:Usuario')->findOneBy(array('username'=>$email));
            if(!is_null($usuario)){
                $password = $this->apiAuth->generatePassword();
                $usuario->setConfirmationToken($password);
                $em->flush();
                die();

                $dataEmail = array('token'=>$password);
                $this->addFlash('success', 'Se ha enviado el correo con éxito');
                $this->sendEmail('recuperarPass',$email,$dataEmail);

            }else{
                return $this->redirectToRoute('error_404');
            }
        }

        return $this->render('usuario/olvide.html.twig', array(
            'usuario' => $usuario,
            'form' => $form->createView(),
        ));
    }
    public function recuperarClaveWebAction(Request $request, $token){

        $em = $this->getDoctrine()->getManager();
        $usuarios = $em->getRepository('AppBundle:Usuario')->porToken($token);
        if(count($usuarios)>0){
            $userId = $usuarios[0]['id'];
            $usuario = $em->getRepository('AppBundle:Usuario')->find($userId);
            $form = $this->createForm('AppBundle\Form\PassType', $usuario);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $usuario->setPlainPassword($usuario->getPassword());
                $usuario->setConfirmationToken('recovered'.$usuario->getId());
                $em->persist($usuario);
                $em->flush();
                //return $this->redirectToRoute('negocio_new');
                return $this->redirectToRoute('usuario_actualizado_pass',array('userId' => $userId));
            }

            return $this->render('usuario/actualizar.html.twig', array(
                'usuario' => $usuario,
                'token' => $token,
                'form' => $form->createView(),
            ));

        }else{
            return $this->redirectToRoute('fos_user_security_login');
        }
    }
    public function registradoAction(){
        return $this->render('usuario/registroExitoso.html.twig', []);
    }
}
