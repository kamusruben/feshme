<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Servicio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Servicio controller.
 *
 */
class ServicioController extends Controller
{
    /**
     * Lists all servicio entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $servicios = $em->getRepository('AppBundle:Servicio')->findAll();

        return $this->render('servicio/index.html.twig', array(
            'servicios' => $servicios,
        ));
    }

    /**
     * Creates a new servicio entity.
     *
     */
    public function newAction(Request $request, $categoriaId)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if(!is_null($user)){
            $em = $this->getDoctrine()->getManager();
            $negocio = $em->getRepository('AppBundle:Negocio')->findOneBy(array('propietario'=>$user));
            $categoria = $em->getRepository('AppBundle:Categoria')->find($categoriaId);
            if(!is_null($categoria)){
                $proId = $categoria->getNegocio()->getPropietario()->getId();
                if($proId == $user->getId()){
                    $servicio = new Servicio();
                    $servicio->setCategoria($categoria);
                    $servicio->setActivo(true);
                    $form = $this->createForm('AppBundle\Form\ServicioType', $servicio);
                    $form->handleRequest($request);

                    if ($form->isSubmitted() && $form->isValid()) {
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($servicio);
                        $em->flush();

                        return $this->redirectToRoute('negocio_show');
                    }

                    return $this->render('servicio/new.html.twig', array(
                        'nombre'    => $negocio->getNombre(),
                        'servicio' => $servicio,
                        'form' => $form->createView(),
                    ));
                }else{
                    die('no coincide'); 
                }
            }else{
                die('no hay acceso');
            }
        }else{
            die('no hay usuario logeado');
        }
    }

    /**
     * Finds and displays a servicio entity.
     *
     */
    public function showAction(Servicio $servicio)
    {
        $deleteForm = $this->createDeleteForm($servicio);

        return $this->render('servicio/show.html.twig', array(
            'servicio' => $servicio,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing servicio entity.
     *
     */
    public function editAction(Request $request, Servicio $servicio)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if(!is_null($user)){
            $em = $this->getDoctrine()->getManager();
            $negocio = $em->getRepository('AppBundle:Negocio')->findOneBy(array('propietario'=>$user));
            if(!is_null($negocio)){
                $deleteForm = $this->createDeleteForm($servicio);
                $editForm = $this->createForm('AppBundle\Form\ServicioType', $servicio);
                $editForm->handleRequest($request);

                if ($editForm->isSubmitted() && $editForm->isValid()) {
                    $this->getDoctrine()->getManager()->flush();

                    return $this->redirectToRoute('negocio_show');
                }

                return $this->render('servicio/edit.html.twig', array(
                    'nombre' => $negocio->getNombre(),
                    'servicio' => $servicio,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                ));
            }
        }
    }

    /**
     * Deletes a servicio entity.
     *
     */
    public function deleteAction(Request $request, Servicio $servicio)
    {
        $form = $this->createDeleteForm($servicio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($servicio);
            $em->flush();
        }

        return $this->redirectToRoute('servicio_index');
    }

    /**
     * Creates a form to delete a servicio entity.
     *
     * @param Servicio $servicio The servicio entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Servicio $servicio)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('servicio_delete', array('id' => $servicio->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
