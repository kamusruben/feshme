<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $tipos= $em->getRepository('AppBundle:Tipo')->findBy(array('activo'=>true));
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'tipos' => $tipos,
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
    public function menuAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $authChecker = $this->container->get('security.authorization_checker');
        $router = $this->container->get('router');
        if ($authChecker->isGranted('ROLE_ADMIN')) {
            $nego= $em->getRepository('AppBundle:Negocio')->findOneBy(array('propietario'=>$user->getId()));
            $nombre = is_null($nego) ? $user->getNombre(): $nego->getNombre();
            return new RedirectResponse($router->generate('negocio_show'), 307);
        } else if ($authChecker->isGranted('ROLE_SUPER')) {
            return new RedirectResponse($router->generate('super_admin'), 307);
        }else if ($authChecker->isGranted('ROLE_USER')) {
            $request->getSession()->invalidate();
            return new RedirectResponse($router->generate('error_404'), 307); 
        }else{
            echo 'ninguno';
        }

        // replace this example code with whatever you need
        return $this->render('default/menu.html.twig', [
            'nombre' => $nombre
        ]);
    }
}

