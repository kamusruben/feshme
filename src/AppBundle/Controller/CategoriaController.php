<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Categoria;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Categoria controller.
 *
 */
class CategoriaController extends Controller
{
    /**
     * Lists all Categoria entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $negocio = $em->getRepository('AppBundle:Negocio')->findOneBy(array('propietario'=>$user->getId()));
        $categorias = $em->getRepository('AppBundle:Categoria')->findBy(array('negocio'=>$negocio));

        return $this->render('categoria/index.html.twig', array(
            'categorias' => $categorias,
        ));
    }

    /**
     * Creates a new Categoria entity.
     *
     */
    public function newAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if(!is_null($user)){
            $em = $this->getDoctrine()->getManager();
            $negocio = $em->getRepository('AppBundle:Negocio')->findOneBy(array('propietario'=>$user));
            $categoria = new Categoria();
            $categoria->setNegocio($negocio);
            $categoria->setActivo(true);
            $form = $this->createForm('AppBundle\Form\CategoriaType', $categoria);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                
                $em->persist($categoria);
                $em->flush();

                return $this->redirectToRoute('negocio_show');
            }

            return $this->render('categoria/new.html.twig', array(
                'nombre'        => $negocio->getNombre(),
                'categoria'     => $categoria,
                'form'          => $form->createView(),
            ));
        }else{
            return $this->redirectToRoute('categoria_index');   
        }
    }

    /**
     * Finds and displays a Categoria entity.
     *
     */
    public function showAction(Categoria $categoria)
    {
        //$deleteForm = $this->createDeleteForm($categoria);
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if(!is_null($user)) {
            $negocio = $em->getRepository('AppBundle:Negocio')->findOneBy(array('propietario' => $user));
            $servicios = $em->getRepository('AppBundle:Servicio')->porCategoria($categoria->getId());
            return $this->render('categoria/show.html.twig', array(
                'nombre'        => $negocio->getNombre(),
                'categoria' => $categoria,
                'servicios' => $servicios,
                //'delete_form' => $deleteForm->createView(),
            ));
        }

    }

    /**
     * Displays a form to edit an existing Categoria entity.
     *
     */
    public function editAction(Request $request, Categoria $categoria)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.token_storage')->getToken()->getUser();
        if(!is_null($user)) {
            $deleteForm = $this->createDeleteForm($categoria);
            $editForm = $this->createForm('AppBundle\Form\CategoriaType', $categoria);
            $editForm->handleRequest($request);
            $negocio = $em->getRepository('AppBundle:Negocio')->findOneBy(array('propietario' => $user));
            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('categoria_edit', array('id' => $categoria->getId()));
            }else{

            }

            return $this->render('categoria/edit.html.twig', array(
                'nombre'        => $negocio->getNombre(),
                'categoria'     => $categoria,
                'edit_form'     => $editForm->createView(),
                'delete_form'   => $deleteForm->createView(),
            ));
        }
    }

    /**
     * Deletes a Categoria entity.
     *
     */
    public function deleteAction(Request $request, Categoria $categoria)
    {
        $form = $this->createDeleteForm($categoria);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($categoria);
            $em->flush();
        }

        return $this->redirectToRoute('categoria_index');
    }

    /**
     * Creates a form to delete a Categoria entity.
     *
     * @param Categoria $categoria The Categoria entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Categoria $categoria)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categoria_delete', array('id' => $categoria->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
