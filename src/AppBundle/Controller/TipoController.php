<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Tipo;
use AppBundle\Util\Funciones;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Security\RestResponse;

/**
 * Tipo controller.
 *
 */
class TipoController extends Controller
{
    private $rest;
    private $funciones;

    public function __construct(RestResponse $rest, Funciones $fun) {
        $this->rest = $rest;
        $this->funciones = $fun;
    }
    /**
     * Lists all tipo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tipos = $em->getRepository('AppBundle:Tipo')->findAll();

        return $this->render('tipo/index.html.twig', array(
            'tipos' => $tipos,
        ));
    }

    /**
     * Creates a new tipo entity.
     *
     */
    public function newAction(Request $request)
    {
        $tipo = new Tipo();
        $form = $this->createForm('AppBundle\Form\TipoType', $tipo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $nombre = $this->funciones->cargarImagen($form,'imagen',$this->getParameter('images_directory'));
            $tipo->setImagen($nombre);
            $em->persist($tipo);
            $em->flush();

            return $this->redirectToRoute('tipo_show', array('id' => $tipo->getId()));
        }

        return $this->render('tipo/new.html.twig', array(
            'tipo' => $tipo,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tipo entity.
     *
     */
    public function showAction(Tipo $tipo)
    {
        $deleteForm = $this->createDeleteForm($tipo);

        return $this->render('tipo/show.html.twig', array(
            'tipo' => $tipo,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tipo entity.
     *
     */
    public function editAction(Request $request, Tipo $tipo)
    {
        $deleteForm = $this->createDeleteForm($tipo);
        $editForm = $this->createForm('AppBundle\Form\TipoType', $tipo);
        $editForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $nombre = $this->funciones->cargarImagen($editForm,'imagen',$this->getParameter('images_directory'));
            $tipo->setImagen($nombre);
            $em->persist($tipo);
            $em->flush();
            return $this->redirectToRoute('super_admin');
        }

        return $this->render('tipo/edit.html.twig', array(
            'tipo' => $tipo,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tipo entity.
     *
     */
    public function deleteAction(Request $request, Tipo $tipo)
    {
        $form = $this->createDeleteForm($tipo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tipo);
            $em->flush();
        }

        return $this->redirectToRoute('tipo_index');
    }

    /**
     * Creates a form to delete a tipo entity.
     *
     * @param Tipo $tipo The tipo entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Tipo $tipo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipo_delete', array('id' => $tipo->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Lists all tipo entities.
     *
     */
    public function inicioAction($id)
    {
        $status = 200;
        if($id == 1){
            $em = $this->getDoctrine()->getManager();
            $tipos = $em->getRepository('AppBundle:Tipo')->findBy(['activo'=>true]);
            foreach($tipos as $tipo){
                
                $n = $em->getRepository('AppBundle:Tipo')->countNegocios($tipo);                
                $menu = array(
                    'id'        =>  $tipo->getId(),
                    'nombre'    =>  $tipo->getNombre(),
                    'imagen'    =>  $tipo->getImagen(),
                    'negocios'  =>  $n
                    );
                $data[] = $menu;
            }
            $respuesta = $this->rest->response($data,$status,'','');
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Categorias');
        }
        return new JsonResponse($respuesta,$status);
    }

    public function agregarAction(Request $request){
        $status = 200;
        $data = json_decode(
            $request->getContent(),
            true
        );

        /*$csrfProvider = $this->container->get('form.csrf_provider');
        $csrfToken = $csrfProvider->generateCsrfToken('unknown');
        var_dump($csrfToken);*/

        $tipo = new Tipo();
        $form = $this->createForm('AppBundle\Form\TipoType', $tipo);
        $form->submit($data);
        //if ($form->isSubmitted() && $form->isValid()) {
        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tipo);
            $em->flush();
            $respuesta = array(
                'data' => 'ok',
                'error' => []
            );
        }else{
            $status = 400;
            $respuesta = array(
                'error' => array('codigo'=>'001','descripcion'=>'Información no encontrada')
            );   
        }
        return new JsonResponse($respuesta,$status);
    }
}
