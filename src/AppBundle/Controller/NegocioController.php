<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Negocio;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Security\RestResponse;
use AppBundle\Util\Funciones;

/**
 * Negocio controller.
 *
 */
class NegocioController extends Controller
{
    private $rest;
    private $funciones;

    public function __construct(RestResponse $rest, Funciones $fun) {
        $this->rest = $rest;
        $this->funciones =$fun;
    }
    /**
     * Lists all negocio entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $negocios = $em->getRepository('AppBundle:Negocio')->findAll();

        return $this->render('negocio/index.html.twig', array(
            'negocios' => $negocios,
        ));
    }

    /**
     * Creates a new negocio entity.
     *
     */
    public function newAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $negocios = $em->getRepository('AppBundle:Negocio')->findOneBy(array('propietario'=>$user));
        if(is_null($negocios)) {
            $negocio = new Negocio();
            $negocio->setPropietario($user);
            $form = $this->createForm('AppBundle\Form\NegocioType', $negocio);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $negocio->setTelefonoMovil(str_replace('-','',$negocio->getTelefonoMovil()));
                $negocio->setTelefonoFijo(str_replace('-','',$negocio->getTelefonoFijo()));
                $logo = $form['logo']->getData();
                $nombre = $this->funciones->cargarImagen($form,'logo',$this->getParameter('images_directory'));
                $negocio->setLogo($nombre);



                // updates the 'brochure' property to store the PDF file name
                // instead of its contents
                //$negocio->setCalificacion(0);
                $em->persist($negocio);
                $em->flush();

                return $this->redirectToRoute('negocio_show', array('id' => $negocio->getId()));
            }

            return $this->render('negocio/new.html.twig', array(
                'nombre' => $user->getNombre(),
                'negocio' => $negocio,
                'form' => $form->createView(),
            ));
        }else{
            return $this->redirectToRoute('negocio_edit', array('id' => $negocios->getId()));
        }
    }

    /**
     * Finds and displays a negocio entity.
     *
     */
    public function showAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        /*if($user->getConfirmationToken() == $user->getEmail()){
            return $this->redirectToRoute('usuario_edit');
        }*/
        $nego= $em->getRepository('AppBundle:Negocio')->findOneBy(array('propietario'=>$user->getId()));
        if(!is_null($nego)){
            $result = $em->getRepository('AppBundle:Negocio')->infoNegocio($nego->getId());
            $negocio = $result[0];
            $categorias = $em->getRepository('AppBundle:Categoria')->porNegocio($nego);
            foreach($categorias as $index => $cat){
                $servicios = $em->getRepository('AppBundle:Servicio')->porCategoria($cat['id']);
                $categorias[$index]['servicios'] = $servicios;
            }
            if(!is_null($negocio)){
                //$deleteForm = $this->createDeleteForm($negocio);


                return $this->render('negocio/show.html.twig', array(
                    'nombre'        => $negocio['nombre'],
                    'negocio'       => $negocio,
                    'categorias'    => $categorias,
                    //'delete_form' => $deleteForm->createView(),
                ));
            }else{
                die('no corresponde');
            }
        }else{
            return $this->redirectToRoute('negocio_new');
        }
    }

    /**
     * Displays a form to edit an existing negocio entity.
     *
     */
    public function editAction(Request $request, Negocio $negocio)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if($negocio->getPropietario()->getId() == $user->getId()){
            $deleteForm = $this->createDeleteForm($negocio);
            $editForm = $this->createForm('AppBundle\Form\NegocioEditType', $negocio);
            $editForm->handleRequest($request);

            if ($editForm->isSubmitted() && $editForm->isValid()) {
                $negocio->setTelefonoMovil(str_replace('-','',$negocio->getTelefonoMovil()));
                $negocio->setTelefonoFijo(str_replace('-','',$negocio->getTelefonoFijo()));
                $nombre = $this->funciones->cargarImagen($editForm,'logo',$this->getParameter('images_directory'));
                if($nombre !== '') {
                    $negocio->setLogo($nombre);
                }else{
                    $negocio->setLogo($negocio->getLogo());
                }
                $this->getDoctrine()->getManager()->flush();
                return $this->redirectToRoute('negocio_show');
            }

            return $this->render('negocio/edit.html.twig', array(
                'nombre' => $negocio->getNombre(),
                'negocio' => $negocio,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
        }else{
            return $this->redirectToRoute('error_404');
        }
    }

    /**
     * Deletes a negocio entity.
     *
     */
    public function deleteAction(Request $request, Negocio $negocio)
    {
        $form = $this->createDeleteForm($negocio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($negocio);
            $em->flush();
        }

        return $this->redirectToRoute('negocio_index');
    }

    /**
     * Creates a form to delete a negocio entity.
     *
     * @param Negocio $negocio The negocio entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Negocio $negocio)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('negocio_delete', array('id' => $negocio->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    public function listadoAction($id)
    {
        $status = 200;
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Tipo')->find($id);
        if(!is_null($entity)){
            try{
                $negocios = $em->getRepository('AppBundle:Negocio')->listadoPorTipo($entity);
                foreach($negocios as $negocio){  
                    $menu = array(
                        'id'            =>  $negocio['id'],
                        'nombre'        =>  $negocio['nombre'],
                        'logo'          =>  $negocio['logo'],
                        'latitud'       =>  $negocio['latitud'],
                        'longitud'      =>  $negocio['longitud'],
                        'calificacion'  =>  $negocio['calificacion'],
                        'principal'     =>  $negocio['principal'],
                        'secundaria'    =>  $negocio['secundaria'],
                        'numeracion'    =>  $negocio['numeracion'],
                        );
                    $data[] = $menu;
                }
                $respuesta = $this->rest->response($data,200,'','');
            }catch(Exception $e){
                $respuesta = $this->rest->response([],500,'004',$e->getMessage());
            }
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Tipo de Servicio');
        }
        return new JsonResponse($respuesta,$status);
    }

    public function infoAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Negocio')->find($id);
        $negocio = $em->getRepository('AppBundle:Negocio')->infoNegocio($id);
        $status = 200;
        if(count($negocio)>0){
            $categorias = $em->getRepository('AppBundle:Categoria')->porNegocio($entity);
            $citas = $em->getRepository('AppBundle:Cita')->porNegocio($entity, false);
            foreach($categorias as $index => $cat){
                $servicios = $em->getRepository('AppBundle:Servicio')->porCategoria($cat['id']);
                $categorias[$index]['servicios'] = $servicios;
            }
            $negocio[0]['citas'] = $citas;
            $negocio[0]['categorias'] = $categorias;
            $respuesta = array(
                'codigo' => 200,
                'data' => $negocio,
                'error' => []
            );
            $respuesta = $this->rest->response($negocio,$status,'','');
        }else{
            $status = 400;
            $respuesta = $this->rest->response([],$status,'001','Negocio');
        }
        return new JsonResponse($respuesta,$status);
    }

    public function agregarAction(Request $request){
        $data = json_decode(
            $request->getContent(),
            true
        );
        $formato = 'Y-m-d';
        $fecha = \DateTime::createFromFormat($formato, '2009-02-15');
        $valores = array();
        $data['time'] = $fecha;
        foreach($data as $k=>$value){
            $valores[$k]=$value;
        }
        //parse_str($data,$output);
        //echo get_class($output);
        //var_dump($output);
        return new JsonResponse(['tipo'=>$valores]);
    }
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
