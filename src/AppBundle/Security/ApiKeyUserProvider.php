<?php
// src/AppBundle/Security/ApiKeyUserProvider.php
namespace AppBundle\Security;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Doctrine;

class ApiKeyUserProvider //implements UserProviderInterface
{
    public function getUsernameForApiKey($apiKey,$em,$asObject)
    {
        // Look up the username based on the token in the database, via
        // an API call, or do something entirely different 
        
        $usuario = $em->getRepository('AppBundle:Usuario')->porApiKey($apiKey,$asObject);
        return $usuario;
    }

    public function loadUserByUsername($username)
    {
        return new User(
            $username,
            null,
            // the roles for the user - you may choose to determine
            // these dynamically somehow based on the user
            ['ROLE_API']
        );
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}
