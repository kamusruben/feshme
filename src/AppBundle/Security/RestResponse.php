<?php
// src/AppBundle/Security/ApiKeyAuthenticator.php
namespace AppBundle\Security;

use AppBundle\Security\ApiKeyUserProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class RestResponse
{

    public function response($data,$status,$errorCode,$objeto)
    {
        $respuesta = array();
        $error = array(
            '001'=>'Objeto '.$objeto.' no encontrado',
            '002'=>'No autorizado',
            '003'=>'Usuario no coincide con la cita',
            '004'=>'Error interno, intente más tarde',
            '005'=>'Nombre de usuario o clave no coinciden',
            '006'=>'El email ya se encuentra registrado',
            '007'=>'Password no coinciden, verifique nuevamente',
            '008'=>'Campo '.$objeto.' no encontrado, es obligatorio',
            '009'=>'Calificación debe ser menor o igual a 5',
        );
        
        switch($status){
            case 200:
                $respuesta = array(
                    'data' => $data
                );
            break;
            case 400:
                $respuesta = array(
                    'error' => array('codigo'=>$errorCode,'descripcion'=>$error[$errorCode])
                );
            break;
            case 403:
                $respuesta = array(
                    'error' => array('codigo'=>$errorCode,'descripcion'=>$error[$errorCode])
                );
            break;
            case 500:
                $respuesta = array(
                    'error' => array('codigo'=>'005', 'descripcion'=>$objeto)
                );
            break;
        }
        return $respuesta;
    }
}
